# Use the official Python image from the Docker Hub
FROM python:3.9-slim

# Set the working directory
WORKDIR /app

# Copy the requirements file into the container
COPY requirements.txt requirements.txt

# Install the required dependencies and create a virtual environment
RUN apt-get update && apt-get install -y python3-venv && \
    python3 -m venv /opt/venv && \
    /opt/venv/bin/pip install --upgrade pip && \
    /opt/venv/bin/pip install -r requirements.txt

# Copy the rest of the application code
COPY . .

# Set the PATH to use the virtual environment
ENV PATH="/opt/venv/bin:$PATH"

# Specify the command to run the application
CMD ["python", "app.py"]
